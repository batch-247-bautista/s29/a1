
db.users.find( { $or: [{ firstName: "Jane" }, { age: 76 }] }, { firstName: 1, lastName: 1, phone: 1, _id: 0} ); 

db.users.find({ $and: [{ age: {$lt: 83} }, { age: {$gt: 75} }] });

db.users.find({ $and: [{ age: {$lt: 22} }, { age: {$gt: 20} }] });

db.users.find({ firstName: { $regex: 'J' } });

db.users.find({ age : { $lte : 21 } });